import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-timer',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})

export class CalculatorComponent implements OnInit {
  
  
arr: any[] = [];
value: any = null;
length: any = 0;
display: boolean = false;
result: any;
min: any;
max: any;
  constructor(
  ) { 
    this.arr = [];
    this.length = 0;
    this.result = 0;
  }

  ngOnInit() {
  }

  addToArray(value) {
    if(value > 0) {
      this.arr.push(value);
      ++this.length;
      if(this.length == 5) {
        this.bubbleSort(this.arr)
      }
      this.value = null;
    }
    else {
      alert('Number has to be more than 0');
      this.value = null;
    }
  }

  bubbleSort(arr) {
    for(let i = 0; i < arr.length-1; ++i) {
      for(let j = 0; j < arr.length-1; ++j) {
        if(arr[j+1] < arr[j]){
          let t = arr[j+1];
          arr[j+1] = arr[j];
          arr[j] = t;
        }
      }
    }
    console.log(this.arr);
    console.log(this.arr[0]);
    this.min = this.arr[0];
    this.max = this.arr[this.arr.length-1];
    this.result = this.max-this.min;
    this.display = true;
  }

  reset() {
    console.log('reseted')
    this.arr = [];
    this.value = null;
    this.length = null;
    this.display = false;
    this.result = null;
    this.min = null;
    this.max = null;
  }

}
