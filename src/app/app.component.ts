import { Component } from '@angular/core';
import { Routes } from '@angular/router';
import { CalculatorComponent } from './calculator/calculator.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

 

  calculator: CalculatorComponent;

  constructor(calculator: CalculatorComponent){
    this.calculator = calculator;
  }

  goToTimer() {
    
  }
}
